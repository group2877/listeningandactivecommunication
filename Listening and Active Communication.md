# **Listening and Active Communication**

# I. Active Listening

## **1. What are the steps/strategies to do Active Listening?**

- Avoid getting distracted by your thoughts and try to focus on the speaker and the topic instead
- Try not to interpret the other person and respond when the speaker has finished talking
- Use door openers to show you are interested, to keep the other person talking
- Show the speaker you are listening with your body language
- If appropriate, take notes during significant conversations
- Paraphrase what the speaker has said to make sure you both are on the same page

# II. Reflective Listening

## **2. According to Fisher's model, what are the key points of Reflective Listening?**

Fisher's model lays out the following key points of Reflective Listening:

- Focus on the conversation by removing or reducing any of distractions
- Genuinely embrace the speaker's perspective in a non-judgmental and empathetic approach
- Mirror the mood of the speaker by completely focusing on the speaker
- Summarize what the speaker said in his own words thereby mirroring the essential concept of the speaker
- Respond to the speaker's specific point rather than beating around the bush
- Repeat the procedure for each subject and swap speaker and listener, if necessary
- Both the speaker and the listener embrace the technique of thoughtful silence rather than idle chatter

# III. Reflection

## **3. What are the obstacles in your listening process?**

Sometimes, my mind gets distracted by other things when the speaker is talking. Also, sometimes I tend to interrupt the speaker before he finishes his topic.

## **4. What can you do to improve your listening?**

- Focus on the conversation with the speaker and eliminate any distractions in doing so.
- Let the speaker finish his topic before I respond.

# IV. Types of Communication

## **5.When do you switch to a Passive communication style in your day-to-day life?**

In my day-to-day life, I switch to the Passive communication style whenever I converse with higher authorities such as supervisors, officers, mentors, cops, popular public figures, etc.

## **6.When do you switch to Aggressive communication styles in your day-to-day life?**

In my day-to-day life, I usually do not engage in aggressive communication with anyone. But when I do communicate aggressively, it is only towards those people who are very close to me where I have the freedom to communicate aggressively such as family, friends, etc.

## **7. When do you switch to Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day-to-day life?**

In my day-to-day life, I switch to Passive Aggressive communication when talking to colleagues, friends, or the people I am close to.

## **8. How can you make your communication assertive?**

For assertive communication, take the following steps:

- Learn to recognize and name the feelings that arise when conversing with someone
- Learn to recognize needs that you want to address to the person that you are conversing with
- Practice with people that you feel comfortable with to test your communication skills
- Be aware of the body language to prevent being aggressive instead of assertive
- Don't wait to speak your needs to a person and address them as soon as possible to prevent giving mixed signals

# References

- https://www.youtube.com/watch?v=rzsVh8YwZEQ
- https://en.wikipedia.org/wiki/Reflective_listening
- https://www.youtube.com/watch?v=BanqlGZSWiI
- https://www.youtube.com/watch?v=vlwmfiCb-vc
